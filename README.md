# econf -- Elliott Slaughter's personal Emacs configuration

This is my personal Emacs configuration tool. The tool was not written
with general purpose use in mind, and as such the settings within
reflect my personal preferences. If you find it (or parts of it)
useful, please feel free to fork it and configure it to your own
tastes (and, if you think your changes might be of interest to a
broader audience, submit a pull request to contribute them back to the
upstream repository).

## Quickstart

See `quickstart.el` for instructions on setting up econf.

## License

The files under `lib` are taken from external projects and have their
own licenses. All other files are provided under the license below:

Copyright (c) 2011-2013, Elliott Slaughter <elliottslaughter@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

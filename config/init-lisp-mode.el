;;;; econf -- Elliott Slaughter's personal Emacs configuration
;;;;
;;;; See README.md for license.

(add-hook
 'lisp-mode-hook
 (lambda ()
   (if (not (featurep 'slime))
       (require 'slime))
   (set (make-local-variable lisp-indent-function)
        'common-lisp-indent-function)))

;;;; econf -- Elliott Slaughter's personal Emacs configuration
;;;;
;;;; See README.md for license.

(dolist (hook (list
               'haskell-mode-hook
               'emacs-lisp-mode-hook
               'lisp-interaction-mode-hook
               'lisp-mode-hook
               'maxima-mode-hook
               'ielm-mode-hook
               ))
  (add-hook hook '(lambda () (paredit-mode t))))

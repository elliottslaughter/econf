;;;; econf -- Elliott Slaughter's personal Emacs configuration
;;;;
;;;; See README.md for license.

(eval-after-load "slime"
  '(progn
     (slime-setup '(slime-fancy slime-asdf slime-banner))
     (setq slime-complete-symbol*-fancy t)
     (setq slime-complete-symbol-function 'slime-fuzzy-complete-symbol)))

(defvar slime-lisp-implementations nil)
(add-to-list 'slime-lisp-implementations '(sbcl ("sbcl")))

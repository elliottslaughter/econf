;; Don't change the font for some headings and titles
(setq-default solarized-use-variable-pitch nil)

;; Don't change size of org-mode headlines (but keep other size-changes)
(setq solarized-scale-org-headlines nil)

;; Disable by default
; (load-theme 'solarized-dark t)

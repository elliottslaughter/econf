;;;; econf -- Elliott Slaughter's personal Emacs configuration
;;;;
;;;; See README.md for license.

(setq econf-base (file-name-directory load-file-name))
(defun econf-path (&rest filenames)
  (apply 'concat econf-base filenames))

(setq econf-recipe-dir (econf-path "recipes/"))
(setq econf-src-dir (econf-path "config/"))

(setq el-get-install-skip-emacswiki-recipes t)

(defun econf-ensure-el-get ()
  (let ((econf-el-get-dir
         (file-name-as-directory
          (or (bound-and-true-p el-get-dir)
              (concat (file-name-as-directory user-emacs-directory)
                      "el-get/el-get/")))))
    (add-to-list 'load-path econf-el-get-dir)
    (unless (require 'el-get nil t)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (add-to-list 'el-get-recipe-path econf-recipe-dir)
    (setq el-get-user-package-directory econf-src-dir)))

(defun econf-byte-compile-file (filename)
  (let ((el-filename (concat filename ".el"))
        (elc-filename (concat filename ".elc")))
    (if (file-newer-than-file-p el-filename elc-filename)
        (byte-compile-file el-filename))
    (load filename)))

(defun econf-load-src (filename)
  (econf-byte-compile-file (econf-path "src/" filename)))

(defvar econf-packages
  '(clojure-mode
    cmake-mode
    ;; company-lean
    ;; cython-mode
    haskell-mode
    ;; lean-mode
    lua-mode
    magit
    org-mode
    paredit
    powershell
    ;; proof-general
    rust-mode
    ;; slime
    solarized-emacs
    regent-mode
    terra-mode
    tuareg-mode
    undo-tree
    yaml-mode
    ))

(defvar econf-sources '(global))

(defun econf-install ()
  (interactive)
  (econf-ensure-el-get)
  (el-get 'sync)
  (el-get 'sync econf-packages)
  (dolist (source-name econf-sources)
    (econf-load-src (symbol-name source-name))))

(defun econf-update ()
  (interactive)
  (el-get-update-all))

(econf-install)

(provide 'econf-helper)

;;; Create a symlink from this file to ~/.emacs.
;;; (~/Library/Preferences/Aquamacs Emacs/Preferences.el for Aquamacs users)

(add-to-list 'load-path (file-name-directory (file-truename load-file-name)))
(require 'econf-helper)

;;; Then open Emacs and type:
;;; M-x econf-install
;;; Follow any additional directions. Restart Emacs when done.

;;; Note that SLIME won't actually be able to connect to Common Lisp
;;; (or Clojure) until you have installed Swank, which isn't included
;;; for obvious reasons (namely that I can't assume you have a Lisp
;;; (or Clojure) compiler installed). Here's where I'd recommend
;;; looking for Swank:
;;;
;;; For Common Lisp: http://www.quicklisp.org/
;;; For Clojure: https://github.com/technomancy/clojure-mode#readme
;;;              (section "SLIME")
;;;
;;; Of course, if you don't have the compilers installed yet, do that first.

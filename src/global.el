;;; Global defaults

(setq-default inhibit-splash-screen t)
(setq-default indent-tabs-mode nil)
(setq-default line-number-mode t)
(setq-default column-number-mode t)

; Globally disable electric-indent-mode in Emacs 24.4
(setq-default electric-indent-mode nil)

(show-paren-mode t)
(electric-indent-mode 0)

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;; International key shortcuts

(define-key key-translation-map [?\C-' ?A] (kbd "Á"))
(define-key key-translation-map [?\C-' ?E] (kbd "É"))
(define-key key-translation-map [?\C-' ?I] (kbd "Í"))
(define-key key-translation-map [?\C-' ?O] (kbd "Ó"))
(define-key key-translation-map [?\C-' ?U] (kbd "Ú"))
(define-key key-translation-map [?\C-' ?Y] (kbd "Ý"))
(define-key key-translation-map [?\C-' ?a] (kbd "á"))
(define-key key-translation-map [?\C-' ?e] (kbd "é"))
(define-key key-translation-map [?\C-' ?i] (kbd "í"))
(define-key key-translation-map [?\C-' ?o] (kbd "ó"))
(define-key key-translation-map [?\C-' ?u] (kbd "ú"))
(define-key key-translation-map [?\C-' ?y] (kbd "ý"))

(define-key key-translation-map [?\C-` ?A] (kbd "À"))
(define-key key-translation-map [?\C-` ?E] (kbd "È"))
(define-key key-translation-map [?\C-` ?I] (kbd "Ì"))
(define-key key-translation-map [?\C-` ?O] (kbd "Ò"))
(define-key key-translation-map [?\C-` ?U] (kbd "Ù"))
(define-key key-translation-map [?\C-` ?a] (kbd "à"))
(define-key key-translation-map [?\C-` ?e] (kbd "è"))
(define-key key-translation-map [?\C-` ?i] (kbd "ì"))
(define-key key-translation-map [?\C-` ?o] (kbd "ò"))
(define-key key-translation-map [?\C-` ?u] (kbd "ù"))

;; Spell checking

(when (eq system-type 'darwin)
  (add-to-list 'exec-path "~/.brew/bin/")
  (setq-default ispell-program-name "aspell"))
